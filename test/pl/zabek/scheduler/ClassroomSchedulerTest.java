/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek.scheduler;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zabek
 */
public class ClassroomSchedulerTest {
    
    public ClassroomSchedulerTest() {
    }

    @Test
    public void schedulerShouldWorkForEmptyList() {
        List<Exercise> emptyList = new ArrayList<>();
        
        int expectedSize = 0;
        int actualSize = ClassroomScheduler.schedule(emptyList).size();
        
        assertEquals(expectedSize, actualSize);
    }
    
    @Test
    public void schedulerShouldWorkForSimpleExerciseList() {
        List<Exercise> exercises = new ArrayList<>();
        Exercise ex1 = new Exercise(1, LocalTime.MIN, LocalTime.of(1, 0));
        exercises.add(ex1);
        Exercise ex2 = new Exercise(2, LocalTime.of(1, 0), LocalTime.of(2, 0));
        exercises.add(ex2);
        Exercise ex3 = new Exercise(3, LocalTime.of(2, 0), LocalTime.of(3, 0));
        exercises.add(ex3);
        Exercise ex4 = new Exercise(4, LocalTime.of(11, 0), LocalTime.of(13, 0));
        exercises.add(ex4);
        
        List<Exercise> actual = ClassroomScheduler.schedule(exercises).get(0).getExercises();
        assertEquals(exercises, actual);
    }
    
    @Test
    public void schedulerShouldWorkForNotSimpleExerciseList() {
        List<Exercise> exercises = new ArrayList<>();
        Exercise ex1 = new Exercise(1, LocalTime.MIN, LocalTime.of(1, 0));
        exercises.add(ex1);
        
        Exercise ex2 = new Exercise(2, LocalTime.of(1, 0), LocalTime.of(3, 0));
        exercises.add(ex2);
        
        Exercise ex3 = new Exercise(3, LocalTime.of(1, 0), LocalTime.of(4, 0));
        exercises.add(ex3);
        
        Exercise ex4 = new Exercise(4, LocalTime.of(2, 0), LocalTime.of(2, 30));
        exercises.add(ex4);

        Exercise ex5 = new Exercise(5, LocalTime.of(3, 0), LocalTime.of(5, 00));
        exercises.add(ex5);
        
        
        List<Classroom> expected = new ArrayList<>();
        
        Classroom class1 = new Classroom(1);
        class1.addExercise(ex1);
        class1.addExercise(ex2);
        expected.add(class1);
        
        Classroom class2 = new Classroom(2);
        class2.addExercise(ex3);
        expected.add(class2);
        
        Classroom class3 = new Classroom(3);
        class3.addExercise(ex4);
        class3.addExercise(ex5);
        expected.add(class3);
        
        
        List<Classroom> actual = ClassroomScheduler.schedule(exercises);
        
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void schedulerShouldWorkForTestExerciseList() {
        List<Exercise> exercises = new ArrayList<>();
        Exercise ex1 = new Exercise(1, LocalTime.of(8, 0), LocalTime.of(10, 0));
        exercises.add(ex1);
        
        Exercise ex2 = new Exercise(2, LocalTime.of(8, 0), LocalTime.of(11, 0));
        exercises.add(ex2);
        
        Exercise ex3 = new Exercise(3, LocalTime.of(9, 0), LocalTime.of(11, 0));
        exercises.add(ex3);
        
        Exercise ex4 = new Exercise(4, LocalTime.of(9, 0), LocalTime.of(11, 0));
        exercises.add(ex4);

        Exercise ex5 = new Exercise(5, LocalTime.of(12, 0), LocalTime.of(14, 00));
        exercises.add(ex5);
        
        Exercise ex6 = new Exercise(6, LocalTime.of(12, 0), LocalTime.of(13, 00));
        exercises.add(ex6);
        
        Exercise ex7 = new Exercise(7, LocalTime.of(11, 0), LocalTime.of(13, 00));
        exercises.add(ex7);
        
        Exercise ex8 = new Exercise(8, LocalTime.of(8, 0), LocalTime.of(11, 00));
        exercises.add(ex8);
        
        Exercise ex9 = new Exercise(9, LocalTime.of(12, 0), LocalTime.of(13, 00));
        exercises.add(ex9);
        
        List<Classroom> expected = new ArrayList<>();
        
        Classroom class1 = new Classroom(1);
        class1.addExercise(ex1);
        class1.addExercise(ex7);
        expected.add(class1);
        
        Classroom class2 = new Classroom(2);
        class2.addExercise(ex2);
        class2.addExercise(ex6);
        expected.add(class2);
        
        Classroom class3 = new Classroom(3);
        class3.addExercise(ex8);
        class3.addExercise(ex9);
        expected.add(class3);
        
        Classroom class4 = new Classroom(4);
        class4.addExercise(ex3);
        class4.addExercise(ex5);
        expected.add(class4);
        
        Classroom class5 = new Classroom(5);
        class5.addExercise(ex4);
        expected.add(class5);
        
        
        List<Classroom> actual = ClassroomScheduler.schedule(exercises);
        
        
        assertEquals(expected, actual);
    }
    
    
}
