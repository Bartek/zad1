/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek.scheduler;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author zabek
 */
public class Classroom {
    private final int id;
    private List<Exercise> exercises = new ArrayList<>();

    public Classroom(int id) {
        this.id = id;
    }
    
    public void addExercise(Exercise exercise){
        exercises.add(exercise);
    }
    
    public LocalTime getLastExerciseEnd(){
        if(exercises.isEmpty()){
            return null;
        }
        
        return exercises.get(exercises.size() - 1).getEnd();
    }

    public int getId() {
        return id;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Classroom other = (Classroom) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.exercises, other.exercises)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Classroom{" + "id=" + id + ", exercises=" + exercises + '}';
    }
    
    
    
}
