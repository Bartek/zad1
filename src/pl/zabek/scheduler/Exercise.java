/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek.scheduler;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.Objects;

/**
 *
 * @author zabek
 */
public class Exercise {
    private final int id;
    private LocalTime start;
    private LocalTime end;

    /**
     * Construct new exercise with hour period
     * @param id exercise number
     * @param start start of period
     * @param end end of period
     * @throws NullPointerException if start or end is null
     *         DateTimeException if start is after end
     */
    public Exercise(int id, LocalTime start, LocalTime end) {
        if(start == null || end == null){
            throw new NullPointerException("Start and end of period cannot be null");
        }
        
        if(start.isAfter(end)){
            throw new DateTimeException("Start time can't be after end time");
        }
        this.id = id;
        this.start = start;
        this.end = end;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        hash = 37 * hash + Objects.hashCode(this.start);
        hash = 37 * hash + Objects.hashCode(this.end);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exercise other = (Exercise) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.start, other.start)) {
            return false;
        }
        if (!Objects.equals(this.end, other.end)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Exercise{" + "id=" + id + ", start=" + start + ", end=" + end + '}';
    }
    
    
}
