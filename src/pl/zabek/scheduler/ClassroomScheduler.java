/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek.scheduler;

import java.time.LocalTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 *
 * @author zabek
 */
public class ClassroomScheduler {

    public static void main(String[] args) {
        System.out.println("Please insert number of exercises");
        Scanner scanner = new Scanner(System.in);
        
        int exercisesCount = scanner.nextInt();
        
        if(exercisesCount < 1){
            return;
        }
        
        System.out.println("Please fill in the hours with time in format of HH:MM");
        
        List<Exercise> exercises = new ArrayList<>(exercisesCount);
        
        for (int i = 1; i <= exercisesCount; i++) {
            System.out.println("Enter the start time of class " + i);
            LocalTime start = LocalTime.parse(scanner.next().trim());
            
            System.out.println("Enter the end time of class " + i);
            LocalTime end = LocalTime.parse(scanner.next().trim());
            
            exercises.add(new Exercise(i, start, end));
        }
        
        System.out.println("Your minimum class coutn is " + minClassroomCountForOptimalSchedule(exercises));
        
        scanner.next();
        scanner.close();
    }
    
    /**
     * Count classrooms required for optimal schedule
     *
     * @param exercises list of exercises
     * @return count of classrooms required
     */
    public static int minClassroomCountForOptimalSchedule(List<Exercise> exercises) {
        return schedule(exercises).size();
    }

    /**
     * Schedule exercises in classrooms
     *
     * @param exercises list of exercises
     * @return classroms with scheduled exercises
     */
    public static List<Classroom> schedule(List<Exercise> exercises) {
        List<Exercise> sortedExercises = exercises.stream()
                .sorted(Comparator.comparing(Exercise::getStart).thenComparing(Exercise::getEnd))
                .collect(Collectors.toList());

        //map of end time of last exercise in classroms
        Map<LocalTime, List<Classroom>> classromsMap = new TreeMap<>();

        List<Classroom> allClassroms = new ArrayList<>();

        for (int i = 0; i < sortedExercises.size(); i++) {
            Exercise exercise = sortedExercises.get(i);

            Optional<LocalTime> firstAvaialableTime = classromsMap.keySet().stream()
                    .filter(k -> !exercise.getStart().isBefore(k))
                    .findAny();

            Classroom classroom;

            if (firstAvaialableTime.isPresent()) {
                //if avaialable classrom exist add new exercise to it
                List<Classroom> avaialableClassroms = classromsMap.get(firstAvaialableTime.get());
                classroom = avaialableClassroms.get(0);
                classroom.addExercise(exercise);

                avaialableClassroms.remove(0);
                if (avaialableClassroms.isEmpty()) {
                    classromsMap.remove(firstAvaialableTime.get());
                }
            } else {
                // if not create new
                classroom = new Classroom(allClassroms.size() + 1);
                allClassroms.add(classroom);
                classroom.addExercise(exercise);
            }

            //update classrom map
            if (classromsMap.containsKey(exercise.getEnd())) {
                classromsMap.get(exercise.getEnd()).add(classroom);
            } else {
                //add new key if not exist yet
                List<Classroom> classList = new LinkedList<>();
                classList.add(classroom);
                classromsMap.put(exercise.getEnd(), classList);
            }
        }

        return allClassroms;
    }

}
